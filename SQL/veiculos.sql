-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Mar-2019 às 15:34
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp6`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculos`
--

CREATE TABLE `veiculos` (
  `id` int(100) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `modelo` varchar(30) NOT NULL,
  `preço` varchar(20) NOT NULL,
  `categoria` varchar(30) NOT NULL,
  `foto` varchar(1000) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `veiculos`
--

INSERT INTO `veiculos` (`id`, `marca`, `modelo`, `preço`, `categoria`, `foto`, `last_modified`) VALUES
(1, 'Fiat', 'Uno 1.0', '40.000,00', 'carros', 'uno2.png', '2019-03-10 14:22:46'),
(2, 'Volkwagen', 'Up 1.0', '38.000,00', 'carros', 'up.png', '2019-03-10 14:23:24'),
(3, 'Chevrolet', 'Onix 1.0', '45.000,00', 'carros', 'onix1.png', '2019-03-10 14:23:42'),
(4, 'Renault', 'Sandero 1.6', '42.000,00', 'carros', 'sandero.png', '2019-03-10 14:24:12'),
(5, 'Yamaha', 'Fazer', '13.000,00', 'motos', 'fazer.png', '2019-03-10 14:27:15'),
(6, 'Honda', 'Twister', '14.000,00', 'motos', 'twister.png', '2019-03-10 14:27:55'),
(7, 'Ford', 'Ecosport 1.6', '65.000,00', 'utilitarios', 'ecosport.png', '2019-03-10 14:28:34'),
(8, 'Jeep', 'Renegade 1.8', '85.000,00', 'utilitarios', 'renegade.png', '2019-03-10 14:29:02'),
(9, 'Honda', 'HRV 2.0', '95.000,00', 'carros', 'hrv.png', '2019-03-10 14:29:28'),
(10, 'Volkwagen', 'Delivery', '135.000,00', 'caminhoes', 'delivery.png', '2019-03-10 14:30:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `veiculos`
--
ALTER TABLE `veiculos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `veiculos`
--
ALTER TABLE `veiculos`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
