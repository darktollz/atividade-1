<?php
    defined ('BASEPATH') OR exit ('No direct script access allowed');

    class Veiculo_painel_model extends CI_Model {

        function __construct(){
            parent::__construct();
        }

        public function salvar($dados){
            if(isset($dados['id']) && $dados['id'] > 0):
                //veiculo já existe, devo editar
                $this->db->where('id', $dados['id']);
                unset($dados['id']);
                $this->db->update('veiculos', $dados);
                return $this->db->affected_rows();
            else:
                //veículo não existe, devo inserir
                $this->db->insert('veiculos', $dados);
                return $this->db->insert_id();
            endif;
        }

        public function get($limit=0, $offset=0){
            if($limit == 0):
                $this->db->order_by('id', 'desc');
                $query = $this->db->get('veiculos');
                if($query->num_rows() > 0):
                    return $query->result();
                else:
                    return NULL;
                endif;
            endif;
        }

        public function get_single($id=0)
        {
            $this->db->where('id', $id);
            $query = $this->db->get('veiculos', 1);
            if($query->num_rows() == 1):
                $row = $query->row();
                return $row;
            else:
                return NULL;
            endif;
        }

        public function get_veiculo($id){
            $this->db->where('id', $id);
            $query = $this->db->get('veiculos', 1);
            if($query->num_rows() == 1):
                return $query->result_array();
            else:
                return NULL;
            endif;
        }

        public function excluir($id=0){
            $this->db->where('id', $id);
            $this->db->delete('veiculos');
            return $this->db->affected_rows();
        }

        
    }