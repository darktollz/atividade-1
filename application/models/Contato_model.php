<?php
    defined ('BASEPATH') OR exit('No direct script access allowed');
    include_once APPPATH.'libraries/Contact.php';

    class Contato_model extends CI_Model{

        public function criar(){
            if(sizeof($_POST) == 0) return;


            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $assunto = $this->input->post('assunto');
            $mensagem = $this->input->post('mensagem');

            $contact = new Contact($nome, $email, $assunto, $mensagem);
    
            $contact->save();
        }

        
    }

    ?>