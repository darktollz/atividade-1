<?php
    defined ('BASEPATH') OR exit('No direct script access allowed');
    include_once APPPATH.'libraries/Contact.php';

    class Contato_painel_model extends CI_Model{

        public function lista(){
            $contact = new Contact();
            //organiza a lista e depois retorna o resultado
            $data = $contact->getAll();
    
            return $data;
        }

        public function ver_mensagem($id){
            $contact = new Contact();
            return $contact->getById($id);
        }

        public function excluir_mensagem($id){
            $contact = new Contact();
            return $contact->excluir($id);
        }

    }