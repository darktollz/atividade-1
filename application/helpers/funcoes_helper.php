<?php
    defined ('BASEPATH') OR exit ('No direct script access alloewed');

    if(!function_exists('set_msg')):
        //seta uma mensagem via session para ser lida posteriormente
        function set_msg($msg=NULL){
            $ci = & get_instance();
            $ci->session->set_userdata('aviso', $msg); 
        }
    endif;

    if(!function_exists('get_msg')):
        //retorna a mensagem definida pela função set_msg
        function get_msg($destroy=TRUE){
            $ci = & get_instance();
            $retorno = $ci->session->userdata('aviso');
            if($destroy) $ci->session->unset_userdata('aviso');
            return $retorno;
        }
    endif;

    if(!function_exists('verifica_login')):
        //verifica se o usuário está logado, caso negativo retorna para outra página
        function verifica_login($redirect='setup/login'){
            $ci = & get_instance();
            if($ci->session->userdata('logged') != TRUE):
                set_msg('<p>Acesso restrito, faça login para continuar.</p>');
                redirect($redirect, 'refresh');
            endif;
        }
    endif;

    if(!function_exists('config_upload')):
        //define as configurações para upload  de imagens/arquivos
        function config_upload($path='./assets/img/', $types='jpg|png', $size=512){
            $config['upload_path'] = $path;
            $config['allowed_types'] = $types;
            $config['max size'] = $size;
            return $config;
        }
    endif;