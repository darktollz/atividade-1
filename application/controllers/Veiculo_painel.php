<?php
defined ('BASEPATH') OR exit ('No direct script access allowed');

    class Veiculo_painel extends CI_Controller {

        function __construct(){
            parent::__construct();
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->model('Option_model', 'option');
            $this->load->model('Veiculo_painel_model', 'veiculo');
        }

        public function index(){
            redirect('veiculo_painel/listar', 'refresh');
        }

        public function listar() {
            //verifica se o usuário estaá logado
            verifica_login();

            //carrega a view
            $dados['titulo'] = 'ClassifiCarros - Listagem de veículos';
            $dados['h2'] = 'Listagem de veículos';
            $dados['tela'] = 'listar';
            $dados['veiculos'] = $this->veiculo->get();
            $this->load->view('painel/veiculos', $dados);
        }

        public function cadastrar() {
            //verifica se o usuário estaá logado
            verifica_login();

            //regras de validação
            $this->form_validation->set_rules('marca', 'MARCA', 'trim|required');
            $this->form_validation->set_rules('modelo', 'MODELO', 'trim|required');
            $this->form_validation->set_rules('preço', 'PREÇO', 'trim|required');
            $this->form_validation->set_rules('categoria', 'CATEGORIA', 'trim|required');
            

             //verifica a validação
             if($this->form_validation->run() == FALSE):
                if(validation_errors()):
                    set_msg(validation_errors());
                endif;
            else:
                $this->load->library('upload', config_upload());
                if($this->upload->do_upload('foto')):
                    //upload foi efetuado
                    $dados_upload = $this->upload->data();
                    $dados_form = $this->input->post();
                    $dados_insert['marca'] = $dados_form['marca'];
                    $dados_insert['modelo'] = $dados_form['modelo'];
                    $dados_insert['preço'] = $dados_form['preço'];
                    $dados_insert['categoria'] = $dados_form['categoria'];
                    $dados_insert['foto'] = $dados_upload['file_name'];

                    //salvar no banco de dados
                    if($id = $this->veiculo->salvar($dados_insert)):
                        set_msg('<p>Veículo cadastrado com sucesso!</p>');
                        redirect('veiculo_painel/listar', 'refresh');
                    else:
                        set_msg('<p>Erro! Veículo não cadastrado!</p>');
                    endif; 
                else:
                    //erro no upload
                    $msg = $this->upload->display_errors();
                    $msg .= '<p>São permitidos arquivos JPG e PNG de até 512KB.</p>';
                    set_msg($msg);
                endif;
            endif;
            
            //carrega a view
            $dados['titulo'] = 'ClassifiCarros - Cadastro de veículos';
            $dados['h2'] = 'Cadastro de veículos';
            $dados['tela'] = 'cadastrar';
            $this->load->view('painel/veiculos', $dados);
        }

        public function excluir(){
            //verifica se o usuário estaá logado
            verifica_login();

            //verifica se foi passado o id do veiculo
            $id = $this->uri->segment(3);
            if($id > 0):
                //id informado, continuar com exclusão
                if($veiculo = $this->veiculo->get_single($id)):
                    $dados['veiculo'] = $veiculo;
                else:
                    set_msg('<p>Veículo inexistente! Escolha um veículo para excluir!</p>');
                    redirect('veiculo_painel/listar', 'refresh');
                endif;
            else:
                set_msg('<p>Você deve escolher um veículo para excluir!</p>');
                redirect('veiculo_painel/listar', 'refresh');
            endif;

            //regras de validação
            $this->form_validation->set_rules('enviar', 'ENVIAR', 'trim|required');

            //verifica a validação
            if($this->form_validation->run() == FALSE):
                if(validation_errors()):
                    set_msg(validation_errors());
                endif;
            else:
                $foto = './assets/img/'.$veiculo->foto;
                if($this->veiculo->excluir($id)):
                    unlink($foto);
                    set_msg('<p>Veículo excluído com sucesso!</p>');
                    redirect('veiculo_painel/listar', 'refresh');
                else:
                    set_msg('<p>Erro! Nenhum veículo foi excluído!</p>');
                endif;
            endif;
            //carrega a view
            $dados['titulo'] = 'ClassifiCarros - Exclusão de veículos';
            $dados['h2'] = 'Exclusão de veículos';
            $dados['tela'] = 'excluir';
            $this->load->view('painel/veiculos', $dados);
    }   

    public function editar(){
        //verifica se o usuário estaá logado
        verifica_login();

        //verifica se foi passado o id do veiculo
        $id = $this->uri->segment(3);
        if($id > 0):
            //id informado, continuar com edição
            if($veiculo = $this->veiculo->get_single($id)):
                $dados['veiculo'] = $veiculo;
                $dados_update['id'] = $veiculo->id;
            else:
                set_msg('<p>Veículo inexistente! Escolha um veículo para editar!</p>');
                redirect('veiculo_painel/listar', 'refresh');
            endif;
        else:
            set_msg('<p>Você deve escolher um veículo para editar!</p>');
            redirect('veiculo_painel/listar', 'refresh');
        endif;

        //regras de validação
            $this->form_validation->set_rules('marca', 'MARCA', 'trim|required');
            $this->form_validation->set_rules('modelo', 'MODELO', 'trim|required');
            $this->form_validation->set_rules('preço', 'PREÇO', 'trim|required');
            $this->form_validation->set_rules('categoria', 'CATEGORIA', 'trim|required');

        //verifica a validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $this->load->library('upload', config_upload());
            if(isset($_FILES['foto']) && $_FILES['foto']['name'] != ''):
                //foi enviada uma imagem, devo fazer o upload
                if($this->upload->do_upload('foto')):
                    //upload foi efetuado
                    $foto_antiga = 'assets/img/'.$veiculo->foto;
                    $dados_upload = $this->upload->data();
                    $dados_form = $this->input->post();
                    $dados_update['marca'] = $dados_form['marca'];
                    $dados_update['modelo'] = $dados_form['modelo'];
                    $dados_update['preço'] = $dados_form['preço'];
                    $dados_update['categoria'] = $dados_form['categoria'];
                    $dados_update['foto'] = $dados_update['file_name'];
                    if($this->veiculo->salvar($dados_update)):
                        unlink($imagem_antiga);
                        set_msg('<p>Veículo alterado com sucesso!</p>');
                        $dados['veiculo']->foto = $dados_update['foto'];
                    else:
                        set_msg('<p>Nenhuma alteração foi salva!</p>');
                    endif;
                else:
                    //erro no upload
                    $msg = $this->upload->display_errors();
                    $msg .= '<p>São permitidos arquivos JPG e PNG de até 512KB.</p>';
                    set_msg($msg);
                endif;
            else:
                //não foi enviada uma imagem pelo form
                $dados_form = $this->input->post();
                $dados_update['marca'] = $dados_form['marca'];
                $dados_update['modelo'] = $dados_form['modelo'];
                 $dados_update['preço'] = $dados_form['preço'];
                $dados_update['categoria'] = $dados_form['categoria'];
                if($this->veiculo->salvar($dados_update)):
                     set_msg('<p>Veículo alterado com sucesso!</p>');
                 else:
                      set_msg('<p>Erro! Nenhuma alteração foi salva!</p>');
                endif;
            endif;
        endif;

        //carrega a view
        $dados['titulo'] = 'ClassifiCarros - Edição de veículos';
        $dados['h2'] = 'Edição de veículos';
        $dados['tela'] = 'editar';
        $this->load->view('painel/veiculos', $dados);
    }

    public function lista_contatos(){
        $this->load->view('painel/header');
        $this->load->view('painel/navbar');
        $this->load->view('painel/contatos');
    }
}