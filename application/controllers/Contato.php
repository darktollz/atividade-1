<?php
    class Contato extends CI_Controller{

        function index(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('Contato_model', 'model');
            $this->model->criar();
            $this->load->view('contato');
            $this->load->view('common/footer');
        }
    }
?>