<?php
defined('BASEPATH') OR exit('NO direct script access allowed');

    class Veiculos extends CI_Controller{

        function index(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('common/footer');
            
        }

        public function exibir($cat){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('Veiculo_model');
            $this->load->view('layout_topo');
            $data = $this->Veiculo_model->veiculo_data($cat);
            for($i = 0; $i < count($data); $i++){
                $v['veiculo'] = $this->load->view('card_veiculo', $data[$i], true);
                $this->load->view('card', $v);
            }
            $this->load->view('layout_rodape');
            $this->load->view('common/footer');
            
        }

        
    }

?>