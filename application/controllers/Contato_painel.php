<?php
defined ('BASEPATH') OR exit ('No direct script access allowed');
include_once APPPATH.'libraries/Contact.php';

    class Contato_painel extends CI_Controller {

        public function listar(){
            $this->load->view('painel/header');
            $this->load->view('painel/navbar');
            $this->load->model('Contato_painel_model', 'contato');
            $data['contato'] = $this->contato->lista();
            $this->load->view('painel/contatos', $data);
            $this->load->view('painel/footer');
        }

        public function ver_msg($id){
            $this->load->view('painel/header');
            $this->load->view('painel/navbar');
            $this->load->model('Contato_painel_model', 'contato');
            $data['contato'] = $this->contato->ver_mensagem($id);
            $this->load->view('painel/mensagem', $data);
            $this->load->view('painel/footer');
        }

        public function excluir_msg($id){
            $this->load->view('painel/header');
            $this->load->view('painel/navbar');
            $this->load->model('Contato_painel_model', 'contato');
            $data['contato'] = $this->contato->excluir_mensagem($id);
            redirect('contato_painel/listar');
        }
    }