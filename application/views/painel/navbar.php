<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark blue-gradient">
  <a class="navbar-brand" href="#">Classificarros</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('home') ?>">Ver site
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">                                                                                
        <a class="nav-link" href="<?php echo base_url('veiculo_painel/listar') ?>">Veiculos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('setup/alterar') ?>">Configurações</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('Contato_painel/listar') ?>">Mensagens</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('setup/logout') ?>">Sair</a>
      </li>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <i class="fas fa-tools"></i>Opções
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="<?php echo base_url('veiculo_painel/cadastrar') ?>">Cadastrar veículo</a>
          <a class="dropdown-item" href="<?php echo base_url('veiculo_painel/listar') ?>" >Editar veículo</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->