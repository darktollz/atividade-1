<?php defined('BASEPATH') OR exit('NO direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Classificarros</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href= <?= base_url('assets/mdb/css/bootstrap.min.css') ?> rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href= <?= base_url('assets/mdb/css/mdb.min.css') ?> rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <!-- CSS exclusivo do painel -->
  <link href= <?= base_url('assets/mdb/css/painel.css') ?> rel="stylesheet">
</head>
<?php $this->load->view('common/navbar'); ?>
<body>
    <div class="row" style="padding: 100px;">
        <div class="coluna col-4 text-center ">&nbsp;</div>
        <div class="coluna col-3 text-center">
        <h2><?php echo $h2; ?></h2>
        <?php 
            if($msg = get_msg()) :
                echo '<div class="msg-box">'.$msg.'</div>';
            endif;
            echo form_open();
            echo form_label('Usuario: ', 'login');
            echo form_input('login', set_value('login'), array('autofocus' => 'autofocus'));
            echo form_label('Senha: ', 'senha');
            echo form_password('senha');
            echo form_submit('enviar', 'Autenticar', array('class' => 'botao'));
            echo form_close();
        ?>
        </div>
        <div class="coluna col3">&nbsp;</div>
    </div>
</body>
</html>
