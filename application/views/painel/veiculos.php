<?php $this->load->view('painel/header');
      $this->load->view('painel/navbar');
?>
<div class="row">
<div class="col-4"></div>
        <div class="coluna col-3 text-center">
        <h2><?php echo $h2; ?></h2>
        <?php 
            if($msg = get_msg()) :
                echo '<div class="msg-box">'.$msg.'</div>';
            endif;

            switch($tela):
                case 'listar':
                    if(isset($veiculos) && sizeof($veiculos) > 0):
                        ?>
                        <table>
                            <thead>
                                <th align="left">Veículo</th>
                                <th align="right">Ações</th>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($veiculos as $linha):
                                ?>
                                <tr>
                                    <td align="center" class="veiculo" style="width: 500px;"><?php echo "$linha->marca $linha->modelo" ?></td>
                                    <td align="center" class="acoes" style="width: 300px;"><?php  echo anchor ('veiculo_painel/editar/'.$linha->id, 'Editar'); ?> | 
                                    <?php  echo anchor ('veiculo_painel/excluir/'.$linha->id, 'Excluir'); ?> | 
                                    <?php  echo anchor ('veiculos/exibir/'.$linha->categoria, 'Ver', array('target'
                                => '_blank')); ?></th>
                                </tr>
                                <?php
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        echo '<div class="msg-box"><p>Nenhum veículo cadastrado!</p></div>';
                    endif;
                    break;
                case 'cadastrar':
                    echo form_open_multipart();
                    echo form_label('Marca:', 'marca');
                    echo form_input('marca', set_value('marca'));
                    echo form_label('Modelo:', 'modelo');
                    echo form_input('modelo', set_value('modelo'));
                    echo form_label('Preço (R$):', 'preço');
                    echo form_input('preço', set_value('preço'));

                    $opcoes = array (
                        'carros' => 'Carros',
                        'motos' => 'Motos',
                        'caminhoes' => 'Caminhoes',
                        'caminhoes' => 'Caminhoes',
                        'utilitarios' => 'Utilitários',
                    );

                    echo form_label('Categoria:', 'categoria');
                    echo form_dropdown('categoria', $opcoes);
                    echo '<br /><br />';
                    echo form_label('Foto do veículo (thumbnail):', 'foto');
                    echo form_upload('foto');
                    
                    echo form_submit('enviar', 'Salvar', array('class' => 'botao'));
                    form_close();                    
                    break;
                case 'editar':
                        $v= $this->veiculo->get_veiculo($this->uri->segment(3));
                        foreach ($v as $cadastro)
                        echo form_open_multipart();
                        echo '<div align="center">';
                        echo form_label('Marca:', 'marca');
                        echo form_input('marca', set_value('marca', $cadastro['marca']));
                        echo form_label('Modelo:', 'modelo');
                        echo form_input('modelo', set_value('modelo', $cadastro['modelo']));
                        echo form_label('Preço (R$):', 'preço');
                        echo form_input('preço', set_value('preço', $cadastro['preço']));
                        echo form_label('Categoria:', 'categoria');
                        echo form_input('categoria', set_value('categoria', $cadastro['categoria']));
                        echo form_label('Foto do veículo (thumbnail):', 'foto');
                        echo form_upload('foto');
                        echo '<br /><div align="center">';
                            echo form_submit('enviar', 'Salvar', array('class' => 'botao'));
                        echo '</div></div>';
                        form_close(); 
                    break;
                case 'excluir':
                $v= $this->veiculo->get_veiculo($this->uri->segment(3));
                foreach ($v as $cadastro)
                echo form_open_multipart();
                echo '<b>Marca: </b>'.$cadastro['marca'].'<br />';
                echo '<b>Modelo: </b>'.$cadastro['modelo'].'<br />';
                echo '<b>Preço: </b> R$ '.$cadastro['preço'].'<br />';
                echo '<b>Categoria: </b>'.$cadastro['categoria'].'<br />';
                echo '<br /><div align="center">';
                    echo form_submit('enviar', 'Excluir', array('class' => 'botao'));
                echo '</div>';
                form_close();       
                    break;
            endswitch;

        ?>
        </div>
    </div>
    <?php $this->load->view('painel/footer'); ?>
