<?php defined('BASEPATH') OR exit('NO direct script access allowed'); ?>
<!-- Grid column -->
<div class="col-lg-3 col-md-6 mb-lg-0 mb-4">
      <!-- Card -->
      <div class="card align-items-center">
        <!-- Card image -->
        <div class="view overlay">
          <img src="<?php echo base_url("assets/img/$foto") ?>" class="card-img-top"
            alt="">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <!-- Card image -->
        <!-- Card content -->
        <div class="card-body text-center">
          <!-- Category & Title -->
          <a href="" class="grey-text">
            <h5><?= $marca ?></h5>
          </a>
          <h5>
            <strong>
              <a href="" class="dark-grey-text"><?= $modelo ?>
                <span class="badge badge-pill danger-color">NEW</span>
              </a>
            </strong>
          </h5>
          <h4 class="font-weight-bold blue-text">
            <strong>R$ <?= $preço ?></strong>
          </h4>
        </div>
        <!-- Card content -->
      </div>
      <!-- Card -->
    </div>
    <!-- Grid column -->
