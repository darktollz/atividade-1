<style>
p{
    background: linear-gradient(40deg, rgba(0,51,199,.3), rgba(209,149,249,.3));
  }
  
  .hm-gradient {
    background: linear-gradient(40deg, rgba(0,51,125,.1), rgba(85,120,249,.3));
  }
  .heading {
    margin: 0 6rem;
    font-weight: 700;
    color: #0064c8;
  }
  .subheading {
    margin: 2.5rem 6rem;
    color: #bcb2c0;
  }
  .btn.btn-margin {
    margin-left: 6rem;
    margin-top: 3rem;
  }
  .btn.btn-lily {
    background: linear-gradient(40deg, rgba(0,51,220,.9), rgba(155,120,249,.8));
  }
  .title {
    margin-top: 6rem;
    margin-bottom: 2rem;
    color: #5d4267;
  }
  .subtitle {
    color: #bcb2c0;
    margin-left: 20%;
    margin-right: 20%;
    margin-bottom: 6rem;
      }
      
  </style>
  

  <!-- Intro -->
  <section class="view">

    <div class="row">

      <div class="col-md-6">

        <div class="d-flex flex-column justify-content-center align-items-center h-100">
          <h1 class="heading display-3">ClassifiCarros</h1>
          <h4 class="subheading font-weight-bold">O melhor lugar para você encontrar o seu novo veículo! </h4>
          <div class="mr-auto">
            <a href="categorias">
              <button type="button" class="btn btn-lily btn-margin btn-rounded">Confira!<i class="fas fa-caret-right ml-3"></i></button>
            </a>
          </div>
        </div>

      </div>

      <div class="col-md-6">

        <div class="view">
          <img src="https://jurosbaixos.com.br/conteudo/wp-content/uploads/sites/2/2018/01/1516624573_image4.png" class="img-fluid" alt="smaple image">
          <div class="mask flex-center hm-gradient">
          </div>
        </div>

      </div>

    </div>

  </section>
  <!-- Intro -->

</header>
<!-- Main navigation -->