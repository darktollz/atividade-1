<!-- Section: Products v.4 -->
<section class="text-center my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">Selecione uma categoria</h2>
  <!-- Section description -->
  <p class="grey-text text-center w-responsive mx-auto mb-5">Temos o veículo certo para a sua necessidade!</p>

  <!-- Grid row -->
  <div class="container text-center">
    <div class="row col-12">

        <!-- Grid column -->
        <div class="col-lg-3 col-md-6 mb-lg-0 mb-4">
        <!-- Collection card -->
        <div class="card collection-card z-depth-1-half">
            <!-- Card image -->
            <a href="<?php echo base_url('veiculos/exibir/carros') ?>">
                <div class="view zoom">
                <img src="https://s2.glbimg.com/-oLwVF55FXXX2ug5_HfrkhYpQ8s=/620x413/top/e.glbimg.com/og/ed/f/original/2017/04/24/056_057_ae624-01.jpg" class="img-fluid"
                    alt="" style="height: 250px;">
                <div class="stripe dark">
                    <p>Carros
                        <i class="fas fa-angle-right"></i>
                    </p>
                </a>
            </div>
            </div>
            <!-- Card image -->
        </div>
        <!-- Collection card -->
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-3 col-md-6 mb-lg-0 mb-4">
        <!-- Collection card -->
        <div class="card collection-card z-depth-1-half">
            <!-- Card image -->
            <a href="<?php echo base_url('veiculos/exibir/motos') ?>">
                <div class="view zoom">
                <img src="https://s2.glbimg.com/QNn0Q1-K8xECWg1luo88syAu3pY=/620x413/e.glbimg.com/og/ed/f/original/2018/07/16/tiger2.jpeg" class="img-fluid"
                    alt="" style="height: 250px;">
                <div class="stripe light">
                    
                    <p>Motos
                        <i class="fas fa-angle-right"></i>
                    </p>
                </a>
            </div>
            </div>
            <!-- Card image -->
        </div>
        <!-- Collection card -->
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-3 col-md-6 mb-md-0 mb-4">
        <!-- Collection card -->
        <div class="card collection-card z-depth-1-half">
            <!-- Card image -->
            <a href="<?php echo base_url('veiculos/exibir/caminhoes') ?>">
                <div class="view zoom">
                <img src="https://www.volvotrucks.com.br/content/dam/volvo/volvo-trucks/markets/brazil/blog/Curiosidades/1860x1050-bndes-disponibiliza-ajuda-online-para-financiamento-2017.jpg/_jcr_content/renditions/1860x1050-bndes-disponibiliza-ajuda-online-para-financiamento-2017-newsintro.jpg" class="img-fluid"
                    alt="" style="height: 250px;">
                <div class="stripe dark">
                    <p>Caminhões
                        <i class="fas fa-angle-right"></i>
                    </p>
                </a>
            </div>
            </div>
            <!-- Card image -->
        </div>
        <!-- Collection card -->
        </div>
        <!-- Grid column -->

        <!-- Fourth column -->
        <div class="col-lg-3 col-md-6">
        <!-- Collection card -->
        <div class="card collection-card z-depth-1-half">
            <!-- Card image -->
            <a href="<?php echo base_url('veiculos/exibir/utilitarios') ?>">
                <div class="view zoom">
                <img src="https://s2.glbimg.com/0_3E8QVzEE3vuB8qW6rZiDOdb6Q=/620x413/e.glbimg.com/og/ed/f/original/2018/07/23/hiluxfrente.jpg" class="img-fluid"
                    alt="" style="height: 250px;">
                <div class="stripe light">
                    <p>Utilitários
                        <i class="fas fa-angle-right"></i>
                    </p>
                </a>
            </div>
            </div>
            <!-- Card image -->
        </div>
        <!-- Collection card -->
        </div>
        <!-- Fourth column -->

    </div>
</div>
  <!-- Grid row -->

</section>
<!-- Section: Products v.4 -->