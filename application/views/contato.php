<!--Section: Contact v.2-->
<section class="mb-4">

    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Fale conosco</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">Como podemos ajudar?</p>
    <div class="row">

        <!--Grid column-->
        <div class="container md-10 text-center">
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="nome" name="nome" class="form-control">
                            <label for="name" class="">Nome</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Email</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="assunto" name="assunto" class="form-control">
                            <label for="subject" class="">Assunto</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="mensagem" name="mensagem" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message">Mensagem</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->
                <div class="text-center text-md-left">
                <button class="btn btn-primary" type="submit">Enviar</a>
            </form>
            </div>
        <!--Grid column-->

    </div>

</section>
<!--Section: Contact v.2-->