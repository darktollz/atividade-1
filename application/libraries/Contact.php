<?php

    class Contact{

        private $nome;
        private $email;
        private $assunto;
        private $mensagem;

        private $db;

        function __construct($nome=null, $email=null, $assunto=null, $mensagem=null){
            $this->nome = $nome;
            $this->email = $email;
            $this->assunto = $assunto;
            $this->mensagem = $mensagem;

            //acesso ao banco de dados
            $ci = &get_instance();
            $this->db = $ci->db;
        }


        //salva no banco de dados
        public function save(){ 
            $sql = "INSERT INTO contatos (nome, email, assunto, mensagem)
            VALUES ('$this->nome', '$this->email','$this->assunto','$this->mensagem')";
            $this->db->query($sql);
        }

        public function getAll(){ //função para ler no banco de dados
                    $sql = "SELECT * FROM contatos";
                    $res = $this->db->query($sql);
                    return $res->result_array();
        }

        public function getById($id){
                    $rs = $this->db->get_where('contatos', "id = $id");
                   //return $rs->result_array()[0];
                    return $rs->row_array();
                }

        public function excluir($id){
            $this->db->where('id', $id);
            $this->db->delete('contatos');
            return $this->db->affected_rows();
        }
    }